<?php 
	header('Content-type: text/plain'); //plain text
	$root =  __DIR__; //current path
	$awardsRoot = $root.'\awards.csv'; //awards file root
	$contractsRoot = $root.'\contracts.csv'; //contract file root
	$awards = read_file($awardsRoot); //read awards.cvs file
	$contracts = read_file($contractsRoot); //reads contracts.cvs file
	$awards1 = $awards;
	unset($awards[0][0]);
	$awards2 = $awards;
	$contracts1 = $contracts;
	unset($awards[0]); //unset awards array index 0
  unset($contracts[0]); //unset contracts array index 0
	$heading_1 = $awards2[0]; //column heading from awards.csv
	$heading_2 = $contracts1[0]; //column heading from contracts.csv
	$finalHeading = array_unique(array_merge($heading_2,$heading_1)); //column headings
	$final_array = array(); //empty array, this array is to write into a file
  $final_array[] = $finalHeading; //array push- data column headings
	 foreach ($contracts as $con) {
	 	// echo $aw[0];die;
	 	$flag1 = 0;
	 	foreach ($awards as $aw) {
	 		if($con[0] == $aw[0]){ //if contract name of award and contract is same
	 			$each_array = array();
	 			$each_array[0] = $con[0];
	 			$each_array[1] = $con[1];
	 			$each_array[2] = $con[2];
	 			$each_array[3] = $con[3];
	 			$each_array[4] = $con[4];
	 			$each_array[5] = $con[5];
	 			$each_array[6] = $con[6];
	 			$each_array[7] = $con[7];
	 			$each_array[8] = $aw[1];
	 			$each_array[9] = $aw[2];
	 			$each_array[10] = $aw[3];
	 			$each_array[11] = $aw[4];
	 			$each_array[12] = $aw[5];
	 			$final_array[] = $each_array;
	 		}else{
	 			if($flag1 >= 1){
	 				$flag1 = $flag1+1;
	 			}
	 			else{
	 				$flag1 = $flag1+1;
	 			}
	 		}
	 	}
	 	if($flag1 != 3){
	 		$each_array = array();
	 		$each_array[0] = $con[0];
	 		$each_array[1] = $con[1];
	 		$each_array[2] = $con[2];
	 		$each_array[3] = $con[3];
	 		$each_array[4] = $con[4];
	 		$each_array[5] = $con[5];
	 		$each_array[6] = $con[6];
	 		$each_array[7] = $con[7];
	 		$each_array[8] = '';
	 		$each_array[9] = '';
	 		$each_array[10] = '';
	 		$each_array[11] = '';
	 		$each_array[12] = '';
	 		$final_array[] = $each_array;
	 	}
	 }

	//write data into new file called final.cvs
	write_file('final.csv', $final_array);

	//calculate total amount of current contracts
	$totalAmount = 0;
	foreach ($final_array as $val) {
		if($val[1] == 'Current' && $val[12] != ''){ //if status is current and amount is not null
			$totalAmount = $val[12]+$totalAmount;
		}
	}
	echo 'Total Amount of current contracts: '.$totalAmount;

	/**
	* read_file function takes parameter of filename
	* initialize empty array
	* reads data from file and stored in array as multidimensional array
	* returns array
	**/
	function read_file($filename){
	  $rows = array();
	  foreach(file($filename, FILE_IGNORE_NEW_LINES) as $lines){
	  	$rows[] = str_getcsv($lines);
	  }
	  return $rows;
	}

	/**
	* write_file function to write datas into a file
	* 
	**/
	function write_file($finalCsv, $rows){
		$file = fopen($finalCsv, 'w');
		foreach ($rows as $row) {
			// print_r($row);die();
			fputcsv($file, $row);
		}
		fclose($file);
	}
 ?>